package ru.dexsys;

import java.util.*;

class Creator {
    private static List<Integer> listX = new ArrayList<>(); //3
    private static List<Integer> listL = new ArrayList<>(); //7
    private static List<Integer> listM = new ArrayList<>(); //21
    private static List<Integer> listAll = new ArrayList<>(); //Все значения
    private static int[] array;

    static private void createArrayByYourself() {
        System.out.println("Enter your numbers and separate them by a space");
        Scanner scanner = new Scanner(System.in);
        String init = scanner.nextLine();

        while (init.charAt(0) == ' ') {
            init = init.substring(1);
        }
        String[] arrayOfStrings = init.split("\\h+");

        int[] newArray = new int[arrayOfStrings.length];

        for (int i = 0; i < newArray.length; i++) {
            try {
                newArray[i] = Integer.parseInt(arrayOfStrings[i]);
            } catch (NumberFormatException e) {
                System.err.println("You enter the incorrect value");
                createArray();
            }
        }
        array = newArray;
        System.out.println("Your array was created by yourselves");
    }

    static private void createArrayByRandom() {
        int numbers = 0;
        System.out.println("Please enter the numbers of integers");
        Scanner scanner1 = new Scanner(System.in);
        try {
            numbers = scanner1.nextInt();
        } catch (InputMismatchException e) {
            System.err.println("You enter the incorrect value");
            createArray();
        }
        int[] someArgs = new int[numbers];
        for (int i = 0; i < numbers; i++) {
            someArgs[i] = (int) (Math.random() * 101);
        }
        array = someArgs;
        System.out.println("Your array was created by random values from 1 to 100");
    }

    static private void createArray() {
        System.out.println("If You want create Array by random value print \"random\" " +
                "\nIf Your want initialise array by your oun values print \"values\"");
        Scanner scanner = new Scanner(System.in);
        String answer = scanner.nextLine();
        switch (answer) {
            case "random":
                createArrayByRandom();
                break;
            case "values":
                createArrayByYourself();
                break;
            default: {
                System.err.println("You enter the incorrect value");
                createArray();
            }
        }
    }

    static private void printArray() {
        System.out.print("Your array -\t\t");
        for (int i : array) {
            System.out.print(i + "\t");
        }
        System.out.println();
    }

    private static void initArray() { //инициализация списков значениями массива
        Arrays.stream(array).filter(i -> i % 3 == 0).forEach(i -> listX.add(i));
        Arrays.stream(array).filter(i -> i % 7 == 0).forEach(i -> listL.add(i));
        Arrays.stream(array).filter(i -> i % 21 == 0).forEach(i -> listM.add(i));
        Arrays.stream(array).forEach(i -> listAll.add(i));
        System.out.println("Your lists have been initialized");
    }

    private static void printType(String typeOfList) {
        switch (typeOfList) {
            case "X":
                System.out.print("List X -\t\t\t");
                if (!listX.isEmpty()) {
                    listX.forEach(i -> System.out.print(i + "\t"));
                } else {
                    System.out.print("List type X is empty");
                }
                break;
            case "L":
                System.out.print("List L -\t\t\t");
                if (!listL.isEmpty()) {
                    listL.forEach(i -> System.out.print(i + "\t"));
                } else {
                    System.out.print("List type L is empty");
                }
                break;
            case "M":
                System.out.print("List M -\t\t\t");
                if (!listM.isEmpty()) {
                    listM.forEach(i -> System.out.print(i + "\t"));
                } else {
                    System.out.print("List type M is empty");
                }
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + typeOfList);
        }
        System.out.println();
    }

    private static void print() {
        printType("X");
        printType("L");
        printType("M");
    }

    private static void anyMore() {
        System.out.print("Any more values -\t");
        listAll.stream()
                .filter(i -> !listL.contains(i))
                .filter(i -> !listX.contains(i))
                .forEach(i -> System.out.print(i + "\t"));
        System.out.println();
    }

    private static void clearType(String typeOfList) {
        switch (typeOfList) {
            case "X":
                listX.clear();
                System.out.println("list X was cleared");
                break;
            case "L":
                listL.clear();
                System.out.println("list L was cleared");
                break;
            case "M":
                listM.clear();
                System.out.println("list M was cleared");
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + typeOfList);
        }
        System.out.println();
    }

    private static void merge() {
        System.out.print("List's merge -\t\t");
        List<Integer> listSum = new ArrayList<Integer>();
        listSum.addAll(listX);
        listSum.addAll(listL);
        listSum.addAll(listM);
        listSum.forEach(i -> System.out.print(i + "\t"));
        listX.clear();
        listL.clear();
        listM.clear();
        System.out.println();
    }

    private static void help() {
        System.out.println("\tcreate array\t- создание массива с выбраным Вами количеством рандомных значений");
        System.out.println("\tprint array \t- вывод массива на печать");
        System.out.println("\tinit array\t\t- инициализация списков набором значений array");
        System.out.println("\tprint \t\t\t- печать всех списков ");
        System.out.println("\tprint type \t\t- печать конкретного списка, где type принимает значения X,S,M (кратные 3,7,21 соответственно)");
        System.out.println("\tanyMore\t\t\t- выводит на экран были ли значения не вошедшие ни в один список, возможные значения true, false");
        System.out.println("\tclear type\t\t- чистка списка , где type принимает значения X,S,M (кратные 3,7,21 соответственно)");
        System.out.println("\tmerge\t\t\t- слить все списки в один вывести на экран и очистить все списки");
        System.out.println("\thelp\t\t\t- вывод справки по командам");
        System.out.println("\texit\t\t\t- выход из программы");
    }

    static void start() {
        Scanner scanner = new Scanner(System.in);
        String command = scanner.nextLine();
        switch (command) {
            case "create array":
                createArray();
                start();
            case "print array":
                printArray();
                start();
            case "init array":
                initArray();
                start();
            case "print type X":
                printType("X");
                start();
            case "print type L":
                printType("L");
                start();
            case "print type M":
                printType("M");
                start();
            case "print":
                print();
                start();
            case "any more":
                anyMore();
                start();
            case "clear type X":
                clearType("X");
                start();
            case "clear type L":
                clearType("L");
                start();
            case "clear type M":
                clearType("M");
                start();
            case "merge":
                merge();
                start();
            case "help":
                help();
                start();
            case "exit":
                System.exit(0);
            default:
                System.err.println("You enter the incorrect data");
                start();
        }
    }

    static void firstStep() {
        System.out.println("Enter Your command (use help for start)");
    }

    static void createArrayAndInitThem() {
        createArray();
        printArray();
        initArray();
        print();
    }
}